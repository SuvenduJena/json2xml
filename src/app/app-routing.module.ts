import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConverterPageComponent } from './page/converter-page/converter-page.component';


const routes: Routes = [
  { path: 'converter', component: ConverterPageComponent },
  { path: '', redirectTo: 'converter', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

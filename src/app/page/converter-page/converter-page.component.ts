import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as JsonToXML from "js2xmlparser";


@Component({
  selector: 'app-converter-page',
  templateUrl: './converter-page.component.html',
  styleUrls: ['./converter-page.component.css']
})
export class ConverterPageComponent implements OnInit {

  xmlfile: any;
  Filedata: any = [];

  filename: string;
  loading: boolean;
  constructor() { }

  ngOnInit(): void {
  }
  public onChange(fileList: FileList): void {
    let file = fileList[0];
    this.filename = fileList[0].name;
    let fileReader: FileReader = new FileReader();
    let self = this;
    fileReader.onloadend = function (x) {
      self.Filedata = fileReader.result;
      self.convertin2xml(JSON.parse(self.Filedata));
    }
    fileReader.readAsText(file);


    //  console.log(JsonToXML.parse("test", fileReader.readAsText(file)));
  }

  convertin2xml(data?) {
    console.log(data)
    // console.log(this.Filedata);
    // let test: string;
    // test = this.Filedata;
    const options = {
      aliasString: "exAlias",
      attributeString: "exAttr",
      cdataKeys: [
        "exCdata",
        "exCdata2"
      ],
      declaration: {
        include: true,
        encoding: "UTF-8",
        // standalone: "yes",
        version: "1.1"
      },
      // dtd: {
      //     include: true,
      //     name: "exName",
      //     sysId: "exSysId",
      //     pubId: "exPubId"
      // },
      format: {
        doubleQuotes: true,
        indent: "\t",
        newline: "\r\n",
        pretty: true
      },
      typeHandlers: {
        "[object Number]": (value: any) => {
          return value + 17;
        }
      },
      valueString: "exVal",
      wrapHandlers: {
        "exArr": () => {
          return "exArrInner";
        }
      }
    };
    //   console.log(JsonToXML.parse("abc", data, options));
    // this.fileForm = new FormData();
    // this.fileForm.append('file', JsonToXML.parse("endObjectSpecification", data, options));
    // console.log(this.fileForm);
    const tempname: string = data.endObjectSpecification;
   const tagnm=  tempname.substring(5,(tempname.length -2));
    console.log(tagnm)
    this.xmlfile = JsonToXML.parse(tagnm, data, options);
  }
  download() {
    //this.loading = true;
    this.toggleLoadingAnimation();
    const blob = new Blob([this.xmlfile],
      { type: "text/xml" }, // must match the Accept type
    );
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    const nm = this.filename.split('.');
    link.download = nm[0]  + ".xml";
    link.click();
    window.URL.revokeObjectURL(link.href);

  }

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => {

      this.loading = false;
      
    }, 3000);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConverterPageComponent } from './page/converter-page/converter-page.component';
import { NbSpinnerModule } from '@nebular/theme';

@NgModule({
  declarations: [
    AppComponent,
    ConverterPageComponent
  ],
  imports: [
    BrowserModule,
    NbSpinnerModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
